import React from "react"
import { Keyboard, KeyboardAvoidingView, Platform, StyleProp, TouchableNativeFeedback, ViewStyle } from "react-native"
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

const DismissKeyboardView: React.FC<{style?: StyleProp<ViewStyle>}> = ({children, ...props}) => (
        <TouchableNativeFeedback onPress={Keyboard.dismiss} accessible={false}>
            <KeyboardAwareScrollView {...props} behavior={Platform.OS === 'android' ? 'position' : 'padding'} style={props.style}>
                {children}
            </KeyboardAwareScrollView>
        </TouchableNativeFeedback>
    )

export default DismissKeyboardView



